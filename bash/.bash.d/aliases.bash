main () {
  alias vi="vim"
  command -v nvim &>/dev/null && alias vi="nvim"
  command -v tmuxinator &>/dev/null && alias mux="tmuxinator"
  alias ll="ls -lA"
  if command -v exa &>/dev/null; then
    alias ls="exa"
    alias ll="exa -la"
    alias lt="exa --long --tree"
  else
    alias ll="ls -lA"
  fi
}

main $@
