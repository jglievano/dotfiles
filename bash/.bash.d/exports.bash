main () {
  export XDG_BIN_HOME=${HOME}/.local/bin
	export XDG_CACHE_HOME=${HOME}/.cache
	export XDG_CONFIG_HOME=${HOME}/.config
	export XDG_DATA_HOME=${HOME}/.local/share
	export XDG_LIB_HOME=${HOME}/.local/lib

  export UNDEFINED=user_undefined
  export OS=$(uname | awk '{print tolower($0)}')
  export LC_CTYPE=en_US.UTF-8
  export LC_ALL=en_US.UTF-8
  export TERM=xterm-256color
  export PASSWORD_STORE_X_SELECTION=primary
  export ANDROID_HOME=${HOME}/android/sdk
  export ANDROID_SDK_ROOT=${HOME}/android/sdk
  export ANDROID_AVD_HOME=${HOME}/.android/avd
  export ALTERNATE_EDITOR=""
  export VISUAL="vim"
  export EDITOR="vim"
  export GOPATH=${HOME}/go
  if [[ ${OS} == "darwin" ]]; then
    export RUST_SRC_PATH=${HOME}/.rustup/toolchains/stable-x86_64-apple-darwin/lib/rustlib/src/rust/src
  else
    export RUST_SRC_PATH=${HOME}/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src
  fi
  export NVM_DIR=${HOME}/.nvm
  command -v ksdiff &>/dev/null && export P4DIFF=$(which ksdiff)
  command -v ksdiff &>/dev/null && export P4MERGE=$(which ksdiff)
}

main $@
