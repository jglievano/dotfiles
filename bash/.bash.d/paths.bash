add_path () {
  if [[ "${PATH/$1}" = "${PATH}" ]]; then
    if [[ -d $1 ]]; then
      export PATH="$1:${PATH}"
    fi
  fi
}

remove_path () {
  export PATH=$(echo -n "${PATH}" | \
      awk -v RS=: -v ORS=: "$0 != \"$1\"" | \
      sed "s/:$//")
}

main () {
  LOCAL_BIN=${HOME}/.local/bin
  add_path /nix/var/nix/profiles/default/bin
  add_path /usr/local/bin
  add_path /usr/local/go/bin
  add_path /usr/local/php5/bin
  add_path ${ANDROID_HOME:?}/platform-tools
  add_path ${ANDROID_HOME:?}/tools
  add_path ${ANDROID_HOME:?}/tools/bin
  add_path ${GOPATH:?}/bin
  add_path ${HOME}/bin
  add_path ${HOME}/chaos/b
  add_path ${HOME}/chaos/b/swift/bin
  add_path ${HOME}/.cargo/bin
  add_path ${HOME}/.homebrew/bin
  add_path ${HOME}/.npm-global/bin
  add_path ${HOME}/.rbenv/bin
  add_path ${HOME}/.rbenv/plugins/ruby-bulid/bin
  add_path ${LOCAL_BIN}
  add_path ${LOCAL_BIN}/flutter/bin
  add_path /Applications/calibre.app/Contents/console.app/Contents/MacOS
}

main $@
