SESSION_TYPE=local
if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
  SESSION_TYPE=ssh
fi

find_root () {
  declare root=$1
  while [[ "${root}" && \
           ! -d "${root}/.hg" && \
           ! -d "${root}/.git" && \
           ! -d "${root}/.citc" ]]; do
    root="${root%/*}"
  done
  printf "${root}"
}

prompt_colors () {
  declare mod="$1" fg="$2" bg="$3" text="$4"
  printf "\[\e[${mod};38;5;${fg};48;5;${bg}m\]${text}\[\e[0m\]"
}

prompt_fg_color () {
  declare fg="$1" text="$2"
  printf "\[\033[0;${fg}m\]${text}\[\033[0m\]"
}

prompt_fg_bold_color () {
  declare fg="$1" text="$2"
  printf "\[\033[1;${fg}m\]${text}\[\033[0m\]"
}

parse_git_branch () {
  declare branch=$(git branch 2> /dev/null | grep -e '\* ' | sed 's/^..\(.*\)/\1/') \
          git_status=$(git status -s)
  if [[ -n $git_status ]]; then
    printf "$(prompt_fg_color 31 "$branch")"
  else
    printf "$(prompt_fg_color 32 "$branch")"
  fi
}

prompt_string () {
  declare EXIT="$1"
  declare root_abbrev=""
  declare path=$(pwd -P)
  declare username="$USER"
  declare hostname="$(hostname)"

  local vcs_toplevel=$(find_root "${path}" 2>/dev/null)
  local vcs_systems=""
  [[ -d "${vcs_toplevel}/.hg" ]] && vcs_systems="${vcs_systems}•hg"
  [[ -d "${vcs_toplevel}/.git" ]] && vcs_systems="${vcs_systems}•git"
  [[ -d "${vcs_toplevel}/.citc" ]] && vcs_systems="${vcs_systems}•citc"
  if [[ -n "${vcs_toplevel}" ]]; then
    root_abbrev="$(basename $vcs_toplevel)"
    path="${path/$vcs_toplevel/}"
  elif [[ "${path}" = "${HOME}"* ]]; then
    root_abbrev="~"
    path="${path/$HOME/}"
  fi
  path="${path#'/'}"
  if [[ "${root_abbrev}" == "" ]]; then
    root_abbrev="/"
  fi

  END_COLOR=70
  if [[ "${EXIT}" != 0 ]]; then
    END_COLOR=9
  fi
  username="$USER"
  hostname="$(hostname)"
  git_dir=$(git rev-parse --git-dir 2> /dev/null)
  printf "\n"
  if [[ -n $git_dir ]]; then
    printf "\xE2\x9C\xAA $(parse_git_branch)\n"
  fi
  printf "[$(prompt_fg_color 33 "${username}")"
  printf "@$(prompt_fg_color 31 "${hostname}")"
  printf " $(prompt_fg_bold_color 34 "${root_abbrev}")"
  printf "$(prompt_fg_color 34 "/${path}")]"
  printf "> "
}

set_prompt () {
  local exit="$?"
  PS1="$(prompt_string $exit)"
}

main () {
  PROMPT_COMMAND="set_prompt"
}

main $@
