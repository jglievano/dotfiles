settermprofile() {
  declare PROFILE="$1"
  echo -e "\033]50;SetProfile=$PROFILE\a"
}

todo() {
  declare DIR="$1"
  find "${DIR}" -type f -print0 | xargs -0 grep "TODO"
}

gsh() {
  settermprofile "Gcloud"
  gcloud compute ssh $@
  settermprofile "Default"
}

osh() {
  settermprofile "Remote"
  ssh $@
  settermprofile "Default"
}

fromhex() {
  hex=${1#"#"}
  r=$(printf '0x%0.2s' "$hex")
  g=$(printf '0x%0.2s' ${hex#??})
  b=$(printf '0x%0.2s' ${hex#????})
  printf '%03d' "$(( (r<75?0:(r-35)/40)*6*6 + \
                     (g<75?0:(g-35)/40)*6   + \
                     (b<75?0:(b-35)/40)     + 16 ))"
}
