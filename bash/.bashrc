# Personal .bashrc file.
# Author: Gabe Lievano

# Code:

source_path () {
  declare path=$1

  # shellcheck source=/dev/null
  if [[ -e "${path}" ]]; then source "${path}"; fi
}

export HISTCONTROL=ignoredups
unset HISTFILE

source_path "${HOME}/.bash.d/functions.bash"
source_path "${HOME}/.bash.d/exports.bash"
source_path "${HOME}/.bash.d/paths.bash"

command -v rbenv >/dev/null && eval "$(rbenv init - sh)"

source_path "${NVM_DIR}/nvm.sh"
source_path "${NVM_DIR}/bash_completion"
source_path "${HOME}/.fzf.bash"
[[ -n "${HOMEBREWPATH}" ]] && \
  source_path "${HOMEBREWPATH:?}/etc/bash_completion"
source_path "${HOME}/.bash.d/aliases.bash"
source_path "${HOME}/.local.bash"
source_path "${HOME}/google-cloud-sdk/path.bash.inc"
source_path "${HOME}/google-cloud-sdk/completion.bash.inc"

# Stop if not running interactively.
[[ $- != *i* ]] && return

source_path "${HOME}/.bash.d/prompt.bash"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

export PATH="$HOME/.cargo/bin:$PATH"
