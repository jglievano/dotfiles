;;; init.el -- My Emacs configuration

;;; Commentary:
;;; Entry point to this configuration.

;;; Code:

(defconst emacs-start-time (current-time))

;; --- Preferences

(defconst gl-is-linux (eq system-type 'gnu/linux))
(defconst gl-is-macos (eq system-type 'darwin))
(defconst gl-is-windows (eq system-type 'windows-nt))

(defconst gl-font-size "14")
(defconst gl-fonts '("Iosevka Term"))

(defconst gl-indent-offset 2)

(add-hook 'after-init-hook
	  `(lambda ()
	     (let ((elapsed
		    (float-time
		     (time-subtract (current-time) emacs-start-time))))
	       (message "Loading %s...done (%.3fs) [after-init]"
			,load-file-name elapsed))) t)

;; --- Initialize package ---

(require 'package)
(package-initialize)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
             '("gnu" . "https://elpa.gnu.org/packages/"))
(add-to-list 'package-archives
	     '("marmalade" . "https://jorgenschaefer.github.io/packages/"))
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives
	     '("melpa-stable" . "https://stable.melpa.org/packages/"))
(add-to-list 'package-archives
             '("org" . "https://orgmode.org/elpa/"))

(setq package-pinned-packages '((gtags . "marmalade")
				(php-extras . "marmalade")))

;; --- Initialize load paths ---

(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
(add-to-list 'custom-theme-load-path (expand-file-name "themes" user-emacs-directory))
(let ((default-directory (expand-file-name "site-lisp/" user-emacs-directory)))
  (normal-top-level-add-subdirs-to-load-path))

;; --- Dumb preferences ---

;; Be better at launch.
(defvar file-name-handler-alist-old file-name-handler-alist)

(setq package-enable-at-startup nil
      file-name-handler-alist nil
      message-log-max 16384
      gc-cons-threshold 402653184
      gc-cons-percentage 0.6
      auto-window-vscroll nil)

(add-hook 'after-init-hook
	  `(lambda ()
	     (setq file-name-handler-alist file-name-handler-alist-old
		   gc-cons-threshold 800000
		   gc-cons-percentage 0.1)
	     (garbage-collect)) t)

;; Obviously...
(column-number-mode)

;; I do not want things appended to the end of `init.el`.
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file 'noerror)

;; Simplify splash screen.
(setq inhibit-splash-screen t
      inhibit-startup-message t
      inhibit-startup-echo-area-message t)

;; Allow full frame height.
(setq frame-resize-pixelwise t)

;; Annoying bell.
(setq visible-bell 1
      ring-bell-function 'ignore)

(setq-default fill-column 80)

;; Get rid of all extra UI.
(if (fboundp 'fringe-mode) (fringe-mode -1))
(if (fboundp 'tooltip-mode) (tooltip-mode -1))
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

;; --- Fonts ---

;(when (window-system)
;  (set-frame-font "Iosevka Term-14"))
;(require 'i-ligatures)

(when (memq window-system '(mac))
  (set-fontset-font t 'unicode "Apple Color Emoji" nil 'prepend))

;; --- Theme ---

(defvar gl--dark-theme t
  "Whether or not dark theme is on.")

(defvar gl--light-themes '(gruvbox-light-hard
			   spacemacs-light)
  "List of light themes to cycle through.")

(defvar gl--dark-themes '(gruvbox-dark-hard
			  spacemacs-dark
			  nord)
  "List of dark themes to cycle through.")

(defun gl-toggle-theme-mode ()
  "Toggles between light/dark mode."
  (interactive)
  (if (get 'gl-toggle-theme-mode 'dark)
      (progn
	(setq gl--dark-theme nil)
	(load-theme 'gruvbox-light-hard t)
	(put 'gl-toggle-theme-mode 'dark nil))
    (progn
      (setq gl--dark-theme t)
      (load-theme 'gruvbox-dark-hard t)
      (put 'gl-toggle-theme-mode 'dark t))))

(defun gl-cycle-color-theme (@n)
  "Toggles between color themes.

Depends on `gl--dark-theme' to know whether to iterate over
`gl--light-themes' or `gl--dark-themes'.
Loosely based on `http://ergoemacs.org/emacs/elisp_toggle_command.html'."
  (interactive "p")
  (let* ((values
	  (if gl--dark-theme
	      gl--dark-themes gl--light-themes))
	 (index-before
	  (if (get 'gl-cycle-color-theme 'state)
	      (get 'gl-cycle-color-theme 'state)
	    0))
	 (index-after (% (+ index-before (length values) @n)
			 (length values)))
	 (curr-value (nth index-before values))
	 (next-value (nth index-after values)))
    (put 'gl-cycle-color-theme 'state index-after)
    (disable-theme curr-value)
    (load-theme next-value t)
    (message "Color theme changed to %s" next-value)))

(load-theme 'gruvbox-dark-hard t)

;; --- Backups ---

(let ((local-dir (expand-file-name ".local/" user-emacs-directory)))
  (setq
   ad-redefinition-action 'accept
   compilation-always-kill t
   compilation-ask-about-save nil
   minibuffer-prompt-properties '(readonly t
					   point-entered minibuffer-avoid-prompt
					   face minibuffer-prompt)
   auto-save-default nil
   crete-lockfiles nil
   make-backup-files nil
   abbrev-file-name (concat local-dir "abbrev.el")
   auto-save-list-file-name (concat local-dir "autosave")
   backup-directory-alist (list (cons "." (concat local-dir "backup/")))
   pcache-directory (concat local-dir "pcache/")
   server-auth-dir (concat local-dir "server/")))

;; --- Use `use-package' ---

(eval-when-compile
  (require 'diminish)
  (require 'use-package)
  (mapc #'(lambda (entry)
	    (define-prefix-command (cdr entry))
	    (bind-key (car entry) (cdr entry)))
	'(("C-h e" . gl-ctrl-h-e-map))))

;; --- Themes ---

(use-package color-theme
  :config (load "color-theme-library"))

;; --- Libraries ---

(use-package ag :defer t)
(use-package autothemer :defer t)

;; --- "essentials" ---

(when (memq window-system '(mac ns x))
  (use-package exec-path-from-shell
    :config (exec-path-from-shell-initialize)))

(when (memq window-system '(mac ns x))
  (use-package counsel-osx-app
    :bind* ("S-M-SPC" . counsel-osx-app)
    :commands counsel-osx-app
    :config
    (setq counsel-osx-app-location
	  (list "/Applications"
		"/Applications/Misc"
		"/Applications/Utilities"
		(expand-file-name "~/Applications")
		(expand-file-name "~/.nix-profile/Applications")
		"/Applications/Xcode.app/Contents/Applications"))))

(use-package ace-jump-mode :defer t)

(use-package ace-mc
  :bind (("<C-m> h" . ace-mc-add-multiple-cursors)
	 ("<C-m> M-h" . ace-mc-add-single-cursor)))

(use-package ace-window :bind* ("<C-return>" . ace-window))

(use-package aggressive-indent
  :diminish
  :hook (emacs-lisp-mode . aggressive-indent-mode))

(use-package align
  :bind (("C-c [" . align-regexp))
  :commands align)

(use-package anki-editor :commands anki-editor-submit)

(use-package aria2 :commands aria2-downloads-list)

(use-package auto-yasnippet
  :after yasnippet
  :bind (("C-c y a" . aya-create)
	 ("C-c y e" . aya-expand)
	 ("C-c y o" . aya-open-line)))

(use-package avy
  :bind* ("C-." . avy-goto-char-timer)
  :config (avy-setup-default))

(use-package avy-zap
  :bind (("M-z" . avy-zap-to-char-dwim)
	 ("M-Z" . avy-zap-up-to-char-dwim)))

(use-package backup-walker
  :commands backup-walker-start)

(use-package beacon
  :diminish
  :hook (after-init-hook . beacon-mode))

(use-package bm
  :bind (("C-c b b" . bm-toggle)
	 ("C-c b n" . bm-next)
	 ("C-c b p" . bm-previous))
  :commands (bm-repository-load
	     bm-buffer-save
	     bm-buffer-save-all
	     bm-buffer-restore)
  :init
  (add-hook 'after-init-hook 'bm-repository-load)
  (add-hook 'find-file-hooks 'bm-buffer-restore)
  (add-hook 'after-revert-hook #'bm-buffer-restore)
  (add-hook 'kill-buffer-hook #'bm-buffer-save)
  (add-hook 'after-save-hook #'bm-buffer-save)
  (add-hook 'vc-before-checkin-hook #'bm-buffer-save)
  (add-hook 'kill-emacs-hook #'(lambda nil
				 (bm-buffer-save-all)
				 (bm-repository-save))))

(use-package bookmark+
  :after bookmark
  :bind ("M-B" . bookmark-bmenu-list)
  :commands bmkp-jump-dired)

(use-package browse-at-remote :bind ("C-c B" . browse-at-remote))

(use-package browse-kill-ring :defer 5 :commands browse-kill-ring)

(use-package browse-kill-ring+
  :after browse-kill-ring
  :commands (browse-kill-ring-default-keybindings))

(use-package bytecomp-simplify :defer 15)

(use-package calc
  :defer t
  :custom
  (math-additional-units
   '((GiB "1024 * MiB" "Giba Byte")
     (MiB "1024 * KiB" "Mega Byte")
     (KiB "1024 * B" "Kilo Byte")
     (B nil "Byte")
     (Gib "1024 * Mib" "Giga Bit")
     (Mib "1024 * Kib" "Mega Bit")
     (Kib "1024 * b" "Kilo Bit")
     (b "B / 8" "Bit")))
  :config (setq math-units-table nil))

(use-package centered-cursor-mode :commands centered-cursor-mode)

(use-package change-inner
  :bind (("M-i" . change-inner)
	 ("M-o M-o" . change-outer)))

(use-package circe :defer t)

(use-package cmake-font-lock :hook (cmake-mode . cmake-font-lock-activate))

(use-package cmake-mode :mode ("CMakeLists.txt" "\\.cmake\\'"))

(use-package col-highlight :commands col-highlight-mode)

(use-package color-moccur
  :commands (isearch-moccur isearch-all isearch-moccur-all)
  :bind (("M-s O" . moccur)
	 :map isearch-mode-map
	 ("M-o" . isearch-moccur)
	 ("M-O" . isearch-moccur-all)))

(use-package command-log-mode
  :bind (("C-c e M" . command-log-mode)
	 ("C-c e L" . clm/open-command-log-buffer)))

(use-package company
  :defer 5
  :diminish
  :config
  (setq company-dabbrev-downcase nil
	company-minimum-prefix-length 2
	company-require-match nil
	company-dabbrev-ignore-case nil
	company-idle-delay 0.25)
  (global-company-mode))

(use-package company-cabal :after (company haskell-cabal))

(use-package company-lsp
  :disabled t
  :after company
  :commands company-lsp)

(use-package company-quickhelp
  :after company
  :bind (:map company-active-map
	      ("C-c ?" . company-quickhelp-manual-begin)))

(use-package compile
  :no-require
  :bind (("C-c c" . compile)
	 ("M-O" . show-compilation))
  :bind (:map compilation-mode-map
	      ("z" . delete-window))
  :preface
  (defun show-compilation ()
    (interactive)
    (let ((it
	   (catch 'found
	     (dolist (buf (buffer-list))
	       (when (string-match "\\*compilation\\*" (buffer-name buf))
		 (throw 'found buf))))))
      (if it
	  (display-buffer it)
	(call-interactively 'compile))))

  (defun compilation-ansi-color-process-output ()
    (ansi-color-process-output nil)
    (set (make-local-variable 'comint-last-output-start)
	 (point-marker)))
  :hook (compilation-filter . compilation-ansi-color-process-output))

(use-package copy-as-format
  :bind (("C-c w m" . copy-as-format-markdown)
	 ("C-c w g" . copy-as-format-slack)
	 ("C-c w o" . copy-as-format-org-mode)
	 ("C-c w r" . copy-as-format-rst)
	 ("C-c w s" . copy-as-format-github)
	 ("C-c w w" . copy-as-format))
  :init (setq copy-as-format-default "github"))

(use-package counsel
  :after ivy
  :diminish
  :bind (("M-x" . counsel-M-x)
	 ("C-x C-f" . counsel-find-file)
	 ("C-c j" . counsel-git-grep)
	 ("C-c k" . counsel-ag)
	 ("C-h f" . counsel-describe-function)
	 ("C-x r b" . counsel-bookmark)
	 ("C-*" . counsel-org-agenda-headlines)))

(use-package counsel-dash :bind ("C-c C-h" . counsel-dash))

(use-package counsel-gtags :after counsel)

(use-package counsel-projectile
  :after (counsel projectile)
  :config (counsel-projectile-mode 1))

(use-package counsel-tramp :commands counsel-tramp)

(use-package crosshairs :bind ("M-o c" . crosshairs-mode))

(use-package crux :bind ("C-c e i" . crux-find-user-init-file))

(use-package csv-mode :mode "\\.csv\\'")

(use-package cursor-chg
  :commands change-cursor-mode
  :config
  (change-cursor-mode 1)
  (toggle-cursor-type-when-idle 1))

(use-package cus-edit
  :bind (("C-c o" . customize-option)
	 ("C-c O" . customize-group)
	 ("C-c F" . customize-face)))

(use-package dash-at-point
  :bind ("C-c D" . dash-at-point)
  :config
  (add-to-list 'dash-at-point-mode-alist
	       '(haskell-mode . "haskell")))

(use-package deadgrep :bind ("M-s g" . deadgrep))

(use-package dedicated :bind ("C-c W" . dedicated-mode))

(use-package deft :bind ("C-, C-," . deft))

(use-package diff-hl
  :commands (diff-hl-mode diff-hl-dired-mode)
  :hook (magit-post-refresh . diff-hl-magit-post-refresh))

(use-package diff-hl-flydiff
  :commands diff-hl-flydiff-mode)

(use-package diff-mode :commands diff-mode)

(use-package diffview
  :commands (diffview-current diffview-region diffview-message))

(use-package dired-toggle
  :bind ("C-c ~" . dired-toggle)
  :preface
  (defun gl-dired-toggle-mode-hook ()
    (interactive)
    (visual-line-mode 1)
    (setq-local visual-line-fringe-indicators '(nil right-curly-arrow))
    (setq-local word-wrap nil))
  :hook (dired-toggle-mode . gl-dired-toggle-mode-hook))

(use-package dired-x :after dired)

(use-package evil
  :config
  (evil-mode t))

(use-package evil-leader
  :after evil
  :config
  (global-evil-leader-mode)
  (evil-leader/set-leader "SPC")
  (evil-leader/set-key
    "SPC x" 'counsel-M-x
    "SPC r" 'fci-mode
    "SPC t t" 'ansi-term
    "SPC t e" 'eshell
    "b f" 'ivy-switch-buffer
    "b k" 'kill-buffer
    "f f" 'counsel-find-file
    "f g" 'counsel-git-grep
    "m s" 'magit-status
    "p" 'projectile-command-map
    "s" 'swiper
    "t t" 'gl-toggle-theme-mode
    "t c" 'gl-cycle-color-theme
    "w D" 'gl-emacs-layout-left-one-third
    "w F" 'gl-emacs-layout-full
    "w G" 'gl-emacs-layout-right-one-third
    "w H" 'gl-emacs-layout-left
    "w L" 'gl-emacs-layout-right
    "w j" 'split-window-below
    "w l" 'split-window-right
    "w w" 'ace-window
    "w x" 'delete-window))

(use-package evil-surround
  :after evil
  :config (global-evil-surround-mode))

(use-package evil-indent-textobject :after evil)

(use-package fill-column-indicator
  :config
  (add-hook 'after-change-major-mode-hook 'fci-mode))

(use-package helpful
  :bind (("C-c e F" . helpful-function)
	 ("C-h e C" . helpful-command)
	 ("C-h e M" . helpful-macro)
	 ("C-h e L" . helpful-callable)
	 ("C-h e S" . helpful-at-point)
	 ("C-h e V" . helpful-variable)))

(use-package ivy
  :bind ("C-c C-r" . ivy-resume)
  :diminish
  :commands ivy-mode
  :init (ivy-mode 1)
  :config
  (setq ivy-re-builders-alist '((t . ivy--regex-fuzzy))))

(use-package magit
  :ensure t
  :hook (git-commit-mode . turn-on-flyspell)
  :init
  (require 'magit)
  (with-eval-after-load 'info
    (info-initialize)
    (add-to-list 'Info-directory-list
		 (expand-file-name "site-lisp/magit/Documentation"
				   user-emacs-directory)))
  :config
  (setq magit-push-always-verify nil
	git-commit-summary-max-length 50))

(use-package paredit
  :diminish paredit
  :init
  (setq lisp-mode-hooks '(clojure-mode-hook
			  emacs-lisp-mode-hook
			  lisp-mode-hook
			  scheme-mode-hook))
  (dolist (hook lisp-mode-hooks)
    (add-hook hook #'enable-paredit-mode)))

(use-package projectile
  :diminish projectile-mode
  :config
  ;; https://github.com/bbatsov/projectile/issues/1323
  (setq projectile-git-submodule-command nil)
  (projectile-global-mode))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package rich-minority
  :config
  (setq rm-blacklist (append rm-blacklist '(" EditorConfig"
					    " yas"
					    " Undo-Tree"
					    " ARev"
					    " ElDoc")))
  (rich-minority-mode 1))

(use-package smart-mode-line
  :config
  (setq sml/theme 'respectful
	sml/no-confirm-load-theme t))

(use-package smex
  :config (smex-initialize))

(use-package swiper
  :bind (("C-s" . swiper)
	 ("C-S-o" . counsel-rhythmbox)))

(use-package lsp-mode :commands lsp)
(use-package lsp-ui :commands lsp-ui-mode)

(use-package which-key
  :diminish which-key-mode
  :config
  (which-key-mode)
  (setq which-key-sort-order #'which-key-prefix-then-key-order
	which-key-sort-uppercase-first nil
	which-key-add-column-padding 1
	which-key-max-display-columns nil
	which-key-min-display-lines 5
	which-key-idle-delay 0.5)
  (set-face-attribute 'which-key-local-map-description-face nil :weight 'bold)
  (which-key-setup-side-window-bottom))

(use-package yasnippet
  :demand t
  :diminish yas-minor-mode
  :bind (("C-c y d" . yas-load-directory)
	 ("C-c y i" . yas-insert-snippet)
	 ("C-c y f" . yas-visit-snippet-file)
	 ("C-c y n" . yas-new-snippet)
	 ("C-c y t" . yas-tryout-snippet)
	 ("C-c y l" . yas-describe-tables)
	 ("C-c y g" . yas/global-mode)
	 ("C-c y m" . yas/minor-mode)
	 ("C-c y r" . yas-reload-all)
	 ("C-c y x" . yas-expand))
  :bind (:map yas-keymap
	      ("C-i" . yas-next-field-or-maybe-expand))
  :mode ("/\\.emacs\\.d/snippets/" . snippet-mode)
  :config
  (yas-load-directory (expand-file-name "snippets" user-emacs-directory))
  (yas-global-mode 1))

;; Setup programming languages...

(require 'i-prog-lang-cc)
(require 'i-prog-lang-go)
(require 'i-prog-lang-js)
(require 'i-prog-lang-python)
(require 'i-prog-lang-ruby)
(require 'i-prog-lang-rust)
(require 'i-prog-lang-swift)
(require 'i-prog-lang-web)
(require 'i-prog-lang-utilities)

;; U

(fset 'yes-or-no-p 'y-or-n-p)
(global-auto-revert-mode t)
(delete-selection-mode t)

;; Layout

(defun gl--emacs-layout-non-fullscreen ()
  (cl-flet ((set-param (p v) (set-frame-parameter (selected-frame) p v)))
    (set-param 'fullscreen nil)))

(defun gl--emacs-layout-resize-height (factor)
  (set-frame-height (selected-frame) (/ (display-pixel-height) factor) nil 'pixelwise))

(defun gl--emacs-layout-resize-width (factor)
  (set-frame-width (selected-frame) (/ (display-pixel-width) factor) nil 'pixelwise))

(defun gl-emacs-layout-left ()
  (interactive)
  (gl--emacs-layout-non-fullscreen)
  (set-frame-position (selected-frame) 0 0)
  (gl--emacs-layout-resize-height 1)
  (gl--emacs-layout-resize-width 2))

(defun gl-emacs-layout-left-one-third ()
  (interactive)
  (gl--emacs-layout-non-fullscreen)
  (set-frame-position (selected-frame) 0 0)
  (gl--emacs-layout-resize-height 1)
  (gl--emacs-layout-resize-width 3))

(defun gl-emacs-layout-right ()
  (interactive)
  (gl--emacs-layout-non-fullscreen)
  (set-frame-position (selected-frame) (/ (display-pixel-width) 2) 0)
  (gl--emacs-layout-resize-height 1)
  (gl--emacs-layout-resize-width 2))

(defun gl-emacs-layout-right-one-third ()
  (interactive)
  (gl--emacs-layout-non-fullscreen)
  (set-frame-position (selected-frame)
		      (truncate (* (display-pixel-width) (/ 2.0 3))) 0)
  (gl--emacs-layout-resize-height 1)
  (gl--emacs-layout-resize-width 3))

(defun gl-emacs-layout-full ()
  (interactive)
  (gl--emacs-layout-non-fullscreen)
  (set-frame-position (selected-frame) 0 0)
  (gl--emacs-layout-resize-height 1)
  (gl--emacs-layout-resize-width 1))

;; org-mode

(require 'i-org-mode)

(require 'i-email)

;; init-utils.el ends here
