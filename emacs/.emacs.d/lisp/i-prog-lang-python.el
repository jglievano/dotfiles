;;; i-prog-lang-python.el --- The Python programming language.

;;; Commentary:

;; Besides being my preferred scripting language, `python-mode' can be
;; used as the mode for bazel files (and probably other DSLs)

;;; Code:

(use-package python-mode
  :mode (("\\.py\\'" . python-mode)
	 ("BUILD\\'" . python-mode)
	 ("WORKSPACE\\'" . python-mode)))

(provide 'i-prog-lang-python)

;;; i-prog-lang-python.el ends here
