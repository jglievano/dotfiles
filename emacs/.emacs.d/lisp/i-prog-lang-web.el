;;; i-prog-lang-web.el --- Web programming languages.

;;; Commentary:

;; Setup for web coding modes. Mostly just `web-mode' and Sass support.
;; `web-mode' covers most of cases for what is needed in web
;; development.

;;; Code:

(defun gl--setup-web-mode ()
  (setq web-mode-markup-indent-offset 2
	web-mode-css-indent-offset 2
	web-mode-code-indent-offset 2))

(use-package pug-mode
  :mode "\\.pug\\'")

(use-package scss-mode
  :mode "\\.s[ac]ss\\'"
  :config (setq scss-compile-at-save nil))

(use-package web-mode
  :mode (("\\.html?\\'" . web-mode)
	 ("\\.tpl\\.php'" . web-mode)
	 ("\\.[agj]sp\\'" . web-mode)
	 ("\\.as[cp]x\\'" . web-mode)
	 ("\\.erb\\'" . web-mode)
	 ("\\.mustache\\'" . web-mode)
	 ("\\.njk\\'" . web-mode)
	 ("\\.hbs\\'" . web-mode))
  :hook (web-mode-hook . gl--setup-web-mode))

(provide 'i-prog-lang-web)

;;; i-prog-lang-web.el ends here
