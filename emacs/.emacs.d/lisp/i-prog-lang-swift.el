;;; i-prog-lang-swift.el --- Apple's Swift programming language.

;;; Commentary:

;; Support for Swift is not-so-good on Emacs compared to Xcode. But
;; Xcode is slow and does not work on all platforms.

;;; Code:

(use-package lsp-sourcekit
  :after lsp-mode
  :config
  (setenv "SOURCEKIT_TOOLCHAIN_PATH"
	  "/Library/Developer/Toolchains/swift-latest.xctoolchain")
  (setq lsp-sourcekit-executable
	(expand-file-name "~/chaos/b/sourcekit-lsp")))

(use-package swift-mode
  :mode "\\.swift\\'"
  :hook (swift-mode . (lambda () (progn (lsp) (setq fill-column 100))))
  :config (setq swift-mode:basic-offset 2))

(provide 'i-prog-lang-swift)

;;; i-prog-lang-swift.el ends here
