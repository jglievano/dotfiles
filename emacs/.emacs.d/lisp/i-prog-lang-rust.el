;;; i-prog-lang-rust.el --- Rust programming language.

;;; Commentary:

;; Still learning this language, but this package provides all I'll
;; probably ever need. Including `cargo' for Rust package management.

;;; Code:

(use-package cargo
  :after rust-mode
  :config (add-hook 'rust-mode-hook 'cargo-minor-mode))

(use-package flycheck-rust
  :after rust-mode
  :config (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))

(use-package rust-mode
  :hook (rust-mode . (lambda () (lsp)))
  :mode "\\.rs\\'")

(provide 'i-prog-lang-rust)

;;; i-prog-lang-rust.el ends here
