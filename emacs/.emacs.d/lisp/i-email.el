;;; i-email.el -- Setup Email stuff.

;;; Commentary:

;; Using offlineimap, mu4e and stmpmail-multi.

;;; Code:

(setq message-citation-line-function 'message-insert-formatted-citation-line
      message-citation-line-format "On %a, %b %d %Y, %f wrote:\n")

(add-to-list 'load-path "~/smtpmail-multi")
(require 'smtpmail-multi)
(require 'smtpmail)

(setq smtpmail-multi-accounts
      (quote
       ((fastmail . ("jglievano@fastmail.com"
		     "smtp.fastmail.com"
		     587
		     "jg@jglievano.com"
		     nil nil nil nil))
	(gmail . ("jglievano@gmail.com"
		  "smtp.gmail.com"
		  587
		  "jglievano@gmail.com"
		  starttls
		  nil nil nil)))))

(setq smtpmail-multi-associations
      (quote
       (("jglievano@gmail.com" gmail)
	("jg@jglievano.com" fastmail))))

(setq smtpmail-multi-default-account (quote gmail)
      message-send-mail-function 'smtpmail-multi-send-it
      smtpmail-debug-info t
      smtpmail-debug-verbose t)

(require 'shr)

(defun shr-render-current-buffer ()
  (shr-render-region (point-min) (point-max)))

(setq mu4e-compose-dont-reply-to-self t
      mu4e-compose-signature-auto-include nil
      mu4e-html2text-command 'shr-render-current-buffer
      mu4e-mu-binary "~/usr/local/mu"
      user-full-name "Gabe Lievano"
      user-mail-address "jg@jglievano.com"
      mu4e-hide-index-messages t
      mu4e-maildir "~/Mail"
      mu4e-sent-folder "/sent"
      mu4e-drafts-folder "/drafts"
      mu4e-trash-folder "/trash"
      mu4e-refile-folder "/archive"
      mu4e-get-mail-command "offlineimap"
      mu4e-update-interval nil)

(setq mu4e-user-mail-address-list (list "jglievano@gmail.com" "jg@jglievano.com"))
(setq message-kill-buffer-on-exit t
      mu4e-view-show-images t
      mu4e-view-image-max-width 800)

(provide 'i-email)

;;; i-email.el ends here
