;;; i-prog-lang-go.el --- The Go language.

;;; Commentary:

;; Setup everything related to go.

;;; Code:

(defun gl--setup-company-go ()
  (set (make-local-variable 'company-backend) '(company-go))
  (company-mode))

(use-package company-go
  :after go-mode
  :hook (go-mode-hook . gl--setup-company-go))

(use-package flymake-go :after go-mode)

(use-package go-mode
  :mode "\\.go\\'"
  :interpreter ("go" . go-mode)
  :config
  (setq gofmt-command "goimports")
  (add-hook 'before-save-hook #'gofmt-before-save)
  (setq go-path (expand-file-name "go" "~/"))
  (setenv "GOPATH" go-path)
  (setenv "PATH" (concat (getenv "PATH") ":" go-path))
  (setq exec-path (append exec-path (list go-path))))

(provide 'i-prog-lang-go)

;;; i-prog-lang-go.el ends here
