;;; i-prog-lang-ruby.el --- Ruby programming language.

;;; Commentary:

;; Lousy language for most of the part, but useful in a lot of DSLs.

;;; Code:

(use-package ruby-mode
  :mode (("\\Gemfile$" . ruby-mode)
	 ("\\.rake$" . ruby-mode)
	 ("\\.gemspec$" . ruby-mode)
	 ("\\.Guardfile$" . ruby-mode)
	 ("\\.Rakefile$" . ruby-mode)
	 ("\\.Podfile$" . ruby-mode)
	 ("\\.Vagrantfile(.local)?$" . ruby-mode)))

(provide 'i-prog-lang-ruby)

;;; i-prog-lang-ruby.el ends here
