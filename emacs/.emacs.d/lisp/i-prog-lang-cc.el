;;; i-prog-lang-cc.el --- The C++ programming language.

;;; Commentary:

;; Covers C++ and C. And anything related and in between.

;;; Code:

(use-package cc-mode
  :mode (("\\.h\\(h?\\|xx\\|pp\\)\\'" . c++-mode)
	 ("\\.m\\'" . c-mode)
	 ("\\.mm\\'" . c++-mode))
  :bind (:map c++-mode-map
	      ("<" . self-insert-command)
	      (">" . self-insert-comamnd))
  :bind (:map c-mode-base-map
	      ("#" . self-insert-command)
	      ("{" . self-insert-command)
	      ("}" . self-insert-command)
	      ("/" . self-insert-command)
	      ("*" . self-insert-command)
	      (";" . self-insert-command)
	      ("," . self-insert-command)
	      (":" . self-insert-command)
	      ("(" . self-insert-command)
	      ("<return>" . newline-and-indent)
	      ("M-q" . c-fill-paragraph)
	      ("M-j"))
  :preface
  (defun gl-c-mode-common-hook ()
    (require 'flycheck)
    (flycheck-mode 1)
    (setq-local flycheck-check-syntax-automatically nil)
    (setq-local flycheck-highlighting-mode nil)

    (set (make-local-variable 'parens-require-spaces) nil)

    (let ((bufname (buffer-file-name)))
      (when bufname
	(cond
	 ((string-match "/ledger/" bufname)
	  (c-set-style "ledger"))
	 ((string-match "/edg/" bufname)
	  (c-set-style "edg"))
	 (t
	  (c-set-style "clang")))))
    (font-lock-add-keywords
     'c++-mode '(("\\<\\(assert\\|DEBUG\\)(" 1 font-lock-warning-face t))))
  :hook (c-mode-common . gl-c-mode-common-hook)
  :config
  (add-to-list
   'c-style-alist
   '("edg"
     (indent-tabs-mode . nil)
     (c-basic-offset . 2)
     (c-comment-only-line-offset . (0 . 0))
     (c-hanging-braces-alist
      . ((substatement-open before after)
	 (arglist-cont-nonempty)))
     (c-offsets-alist
      . ((statement-block-intro . +)
	 (knr-argdecl-intro . 5)
	 (substatement-open . 0)
	 (substatement-label . 0)
	 (label . 0)
	 (case-label . +)
	 (statement-case-open . 0)
	 (statement-cont . +)
	 (arglist-intro . +)
	 (arglist-close . +)
	 (inline-open . 0)
	 (brace-list-open . 0)
	 (topmost-intro-cont
	  . (first c-lineup-topmost-intro-cont
		   c-lineup-gnu-DEFUN-intro-cont))))
     (c-special-indent-hook . c-gnu-impose-minimum)
     (c-block-comment-prefix . "")))
  (add-to-list
   'c-style-alist
   '("ledger"
     (indent-tabs-mode . nil)
     (c-basic-offset . 2)
     (c-comment-only-line-offset . (0 . 0))
     (c-hanging-braces-alist
      . ((substatement-open before after)
	 (arglist-cont-nonempty)))
     (c-offsets-alist
      . ((statement-block-intro . +)
	 (knr-argdecl-intro . 5)
	 (substatement-open . 0)
	 (substatement-label . 0)
	 (label . 0)
	 (case-label . 0)
	 (statement-case-open . 0)
	 (statement-cont . +)
	 (arglist-intro . +)
	 (arglist-close . +)
	 (inline-open . 0)
	 (brace-list-open . 0)
	 (topmost-intro-cont
	  . (first c-lineup-topmost-intro-cont
		   c-lineup-gnu-DEFUN-intro-cont))))
     (c-special-indent-hook . c-gnu-impose-minimum)
     (c-block-comment-prefix . "")))
  (add-to-list
   'c-style-alist
   '("clang"
     (indent-tabs-mode . nil)
     (c-basic-offset . 2)
     (c-comment-only-line-offset . (0 . 0))
     (c-hanging-braces-alist
      . ((substatement-open before after)
	 (arglist-cont-nonempty)))
     (c-offsets-alist
      . ((statement-block-intro . +)
	 (knr-argdecl-intro . 5)
	 (substatement-open . 0)
	 (substatement-label . 0)
	 (label . 0)
	 (case-label . 0)
	 (statement-case-open . 0)
	 (statement-cont . +)
	 (arglist-intro . +)
	 (arglist-close . +)
	 (inline-open . 0)
	 (brace-list-open . 0)
	 (topmost-intro-cont
	  . (first c-lineup-topmost-intro-cont
		   c-lineup-gnu-DEFUN-intro-cont))))
     (c-special-indent-hook . c-gnu-impose-minimum)
     (c-block-comment-prefix . ""))))

(provide 'i-prog-lang-cc)

;;; i-prog-lang-cc.el ends here
