;;; i-prog-lang-utilities.el --- Utility programming languages.

;;; Commentary:

;; Very high level programming languages like Markdown, Bash,
;; Fish, Toml, Yaml.

;;; Code:

(defun gl--setup-sh-mode ()
  (setq sh-basic-offset 'tab-width
	sh-indentation 'tab-width))
(add-hook 'sh-mode-hook 'gl--setup-sh-mode)

(use-package markdown-mode :mode "\\.md\\'")
(use-package fish-mode :mode "\\.fish\\'")
(use-package nix-mode :mode "\\.nix\\'")
(use-package toml-mode :mode "\\.toml\\'")
(use-package yaml-mode :mode "\\.ya?ml\\'")

(provide 'i-prog-lang-utilities)

;;; i-prog-lang-utilities.el ends here
