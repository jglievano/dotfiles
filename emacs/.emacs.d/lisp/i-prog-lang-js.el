;;; i-prog-lang-js.el --- JavaScript and JSON.

;;; Commentary:

;; This covers JavaScript and JSON because they are so related. I wonder
;; if this should also be part of the "web" programming languages
;; package. Is left independently because JS does not necessarily have
;; to be "web".

;;; Code:

(defun gl--setup-json-mode ()
  (make-local-variable 'js-indent-level)
  (setq js-indent-level 2))

(defun gl--setup-js2-mode ()
  (setq js2-basic-offset 2))

(use-package json-reformat :defer t)
(use-package json-snatcher :defer t)
(use-package json-mode
  :mode "\\.json\\'"
  :hook (json-mode-hook . gl--setup-json-mode))

(use-package js2-mode
  :mode "\\.js\\'"
  :interpreter ("node" . js2-mode)
  :hook (js2-mode-hook . gl--setup-js2-mode))

(provide 'i-prog-lang-js)

;;; i-prog-lang-js.el ends here
