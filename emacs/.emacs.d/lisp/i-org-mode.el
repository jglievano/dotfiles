;;; i-org-mode.el --- Setup `org-mode'.

;;; Commentary:

;; Use org-mode for notes, tasks, journaling and agenda.

;;; Code:

(require 'org)
(require 'org-agenda)

(define-key global-map (kbd "C-c l") 'org-store-link)
(define-key global-map (kbd "C-c a") 'org-agenda)
(define-key global-map (kbd "C-c c") 'org-capture)

(setq org-ellipsis "↴"
      org-src-fontify-natively t
      org-log-done t
      org-log-into-drawer t
      org-edit-timestamp-down-means-later t
      org-hide-emphasis-markers nil
      org-catch-invisible-edits 'show
      org-export-coding-system 'utf-8
      org-fast-tag-selection-single-key 'expert
      org-html-validation-link nil
      org-export-kill-product-buffer-when-displayed t
      org-tags-column 80)

(setq org-agenda-files '("~/Dropbox/org/todo.org")
      org-directory "~/Dropbox/org"
      org-mobile-inbox-for-pull "~/Dropbox/org/inbox.org"
      org-mobile-directory "~/Dropbox/Apps/MobileOrg"
      org-mobile-files '("~/Dropbox/org")
      org-support-shift-select t)

;; Actionable items are tactical or strategic.
;; Tasks are tactical. They have TODO/NEXT for call to action, and DONE/CANCELED
;;   for completion.
;; Decisions are strategic. They have DECIDE/CONSIDER for call to action, and
;;   PASS/REJECTED for completion.
(setq org-todo-keywords
      '((sequence "TODO(t)" "NEXT(n)" "WIP(w)" "|" "DONE(d!/!)" "CANCELED(c@/!)")
	(sequence "DECIDE(e)" "CONSIDER(o)" "|" "PASS(p!/!)" "REJECTED(r@/!)")))

(setq org-capture-templates
      `(("t" "todo" entry (file "")
	 "* TODO %?\n%U\n" :clock-resume t)
	("d" "decision" entry (file "")
	 "* DECIDE %?\n:PROPERTIES:\n:When: %T\n:END:\n" :clock-resumt t)))

(provide 'i-org-mode)

;;; i-org-mode.el ends here
