#!/usr/bin/env bash

while IFS= read -r pkg; do
  name=$(echo $pkg | tr -s ' ' | cut -d ' ' -f 1)
  url=$(echo $pkg | tr -s ' ' | cut -d ' ' -f 2)
  ./emacs-add-subtree.sh $name $url
done < "emacs-packages.in"
