#!/usr/bin/env python3

from optparse import OptionParser

def print_from_hex(hexa):
    hexa = hexa.lstrip("#")
    lh = len(hexa)
    rgb = tuple(int(hexa[i:i + lh // 3], 16) for i in range(0, lh, lh // 3))
    print("red: %d, green: %d, blue: %d" % (rgb[0], rgb[1], rgb[2]))
    print("red: %.2f, green: %.2f, blue: %.2f" % (rgb[0]/255, rgb[1]/255, rgb[2]/255))
    print("UIColor(red: %.2f, green: %.2f, blue: %.2f, alpha: 1.0)" % (rgb[0]/255, rgb[1]/255, rgb[2]/255))

def main():
    parser = OptionParser()
    parser.add_option("-x", "--hex", dest="hexa",
            help="hexadecimal value")
    (options, args) = parser.parse_args()

    print(options.hexa)
    if options.hexa is not None:
        print_from_hex(options.hexa)

if __name__ == "__main__":
    main()
