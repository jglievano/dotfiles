#!/usr/bin/env python3

from optparse import OptionParser

def hex_to_rgb(value):
    value = value.lstrip('#')
    lv = len(value)
    return tuple(int(value[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))

def main():
    parser = OptionParser()
    parser.add_option("-x", "--hexa", dest="hexa",
                      help="Hexadecimal value")
    (options, args) = parser.parse_args()
    if options.hexa != None:
        rgb = hex_to_rgb(options.hexa)
        print('r: %d, g: %d, b: %d' % (rgb[0], rgb[1], rgb[2]))
        print('UIColor(red: %.2f, green: %.2f, blue: %.2f, alpha: 1)' %
                (rgb[0] / 255, rgb[1] / 255, rgb[2] / 255))

if __name__ == '__main__':
    main()
