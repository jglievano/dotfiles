#!/usr/bin/env bash

AQUA=$(tput setaf 30)
GREEN=$(tput setaf 64)
RED=$(tput setaf 203)
RST=$(tput sgr0)

INFO="${AQUA}[dotfiles]:${RST}"
DIFF="diff -u --color=always"

command -v colordiff >/dev/null 2>&1 && DIFF="colordiff"

function check_diff {
  STEP_STATUS=0
  if ! cmp $1 $2 >/dev/null 2>&1; then
    echo "${INFO} Difference found between $1 and $2:"
    $DIFF $1 $2
    echo -n "${INFO} Replace $2(u), $1(r), do nothing(n):"
    read -n 1 action
    echo
    if [ "$action" == "r" ]; then
      rsync -zvh $2 $1
      STEP_STATUS=$?
    elif [ "$action" == "u" ]; then
      rsync -zvh $1 $2
      STEP_STATUS=$?
    else
      STEP_STATUS=1
    fi
  fi

  if [ $STEP_STATUS -eq 0 ]; then
    echo "${INFO}   ${GREEN}✓${RST} $1."
  else
    echo "${INFO}   ${RED}✗${RST} $1."
  fi
}

function print_result {
  if [ $1 -ne 0 ]; then
    echo "${INFO}   ${RED}✗${RST} $2."
  else
    echo "${INFO}   ${GREEN}✓${RST} $2."
  fi
}

check_diff bashrc ~/.bashrc
check_diff bash_prompt.sh ~/.bash_prompt.sh
check_diff zshrc.zsh ~/.zshrc
mkdir -p ~/.custom-omzsh/themes
check_diff custom-omzsh/themes/jgl.zsh-theme ~/.custom-omzsh/themes/jgl.zsh-theme
mkdir -p ~/.config/fish
check_diff config/fish/config.fish ~/.config/fish/config.fish
check_diff gitconfig ~/.gitconfig
check_diff gitignore ~/.gitignore
check_diff hgrc ~/.hgrc
check_diff shrc ~/.shrc
check_diff tmux.conf ~/.tmux.conf
check_diff tmux-macos.conf ~/.tmux-macos.conf
mkdir -p ~/.tmux/theme
check_diff tmux/theme/theme-01.conf ~/.tmux/theme/theme-01.conf
check_diff Xmodmap ~/.Xmodmap
mkdir -p ~/.local/bin
check_diff local/bin/em ~/.local/bin/em
check_diff local/bin/tm ~/.local/bin/tm
check_diff local/bin/tm-ls ~/.local/bin/tm-ls
check_diff local/bin/git-rm-submodule ~/.local/bin/git-rm-submodule
check_diff local/bin/git-root ~/.local/bin/git-root
mkdir -p ~/.vim
mkdir -p ~/.vim/{autoload,syntax}
check_diff vim/bundles.vim ~/.vim/bundles.vim
check_diff vim/keybindings.vim ~/.vim/keybindings.vim
check_diff vim/vimrc.vim ~/.vim/vimrc.vim
check_diff vim/autoload/plug.vim ~/.vim/autoload/plug.vim
check_diff vim/syntax/haskell.vim ~/.vim/syntax/haskell.vim
[ -e "$HOME/.vimrc" ] || ln -s ~/.vim/vimrc.vim ~/.vimrc

echo -n "${INFO} Do you want to update Vim plugins? [yN]: "
read -n 1 action
echo
if [ "$action" == "y" -o "$action" == "Y" ]; then
  vim +PlugInstall +qall
  echo "${INFO}   ${GREEN}✓${RST} Vim plugins."
elif [ "$action" == "n" -o "$action" == "N" ]; then
  echo "${INFO}   ${RED}✗${RST} Vim plugins."
fi

echo "${INFO} Install Oh My Zsh."
if [ -d ~/.oh-my-zsh ]; then
  STEP_STATUS=0
else
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
  STEP_STATUS=$?
fi
print_result $STEP_STATUS oh-my-zsh

echo "${INFO} Installing nvm."
if [ -f ~/.nvm/nvm.sh ]; then
  STEP_STATUS=0
else
  curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash
  STEP_STATUS=$?
fi
print_result $STEP_STATUS nvm

echo "${INFO} Installing tpm."
if [ -d ~/.tmux/plugins/tpm ]; then
  STEP_STATUS=0
else
  git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
  STEP_STATUS=$?
fi
print_result $STEP_STATUS tpm

echo "${INFO} Starting platform-specific installation."
if [ "$(uname)" == "Darwin" ]; then
  ./installer/os/macos/setup.sh
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
  ./installer/os/linux/setup.sh
fi

echo "${INFO} Checking for dependencies."
if [ -f ~/.config/fish/functions/fisher.fish >/dev/null ]; then
  STEP_STATUS=0
else
  curl -Lo ~/.config/fish/functions/fisher.fish --create-dirs https://git.io/fisher
  STEP_STATUS=$?
fi
print_result $STEP_STATUS fisherman

echo "${INFO} ${GREEN}Done.${RST}"
