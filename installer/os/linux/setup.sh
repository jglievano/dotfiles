#!/usr/bin/env bash

AQUA=$(tput setaf 30)
GREEN=$(tput setaf 64)
RED=$(tput setaf 203)
RST=$(tput sgr0)

INFO="${AQUA}[dotfiles]:${RST}"

command -v colordiff >/dev/null 2>&1 && DIFF="colordiff"

function check_diff {
  STEP_STATUS=0
  if ! cmp $1 $2 >/dev/null 2>&1; then
    echo "${INFO} Difference found between $1 and $2:"
    $DIFF $1 $2
    echo -n "${INFO} Replace $2(u), $1(r), do nothing(n):"
    read -n 1 action
    echo
    if [ "$action" == "r" ]; then
      rsync -zvh $2 $1
      STEP_STATUS=$?
    elif [ "$action" == "u" ]; then
      rsync -zvh $1 $2
      STEP_STATUS=$?
    else
      STEP_STATUS=1
    fi
  fi

  if [ $STEP_STATUS -eq 0 ]; then
    echo "${INFO}   ${GREEN}✓${RST} $1."
  else
    echo "${INFO}   ${RED}✗${RST} $1."
  fi
}

check_diff xinitrc ~/.xinitrc
check_diff Xresources ~/.Xresources
mkdir -p ~/.config/{gtk-3.0,i3/blocks,rofi,termite}
check_diff config/gtk-3.0/gtk.css ~/.config/gtk-3.0/gtk.css
check_diff config/i3/config ~/.config/i3/config
check_diff config/i3/blocks/audio ~/.config/i3/blocks/audio
check_diff config/i3/blocks/bandwidth ~/.config/i3/blocks/bandwidth
check_diff config/i3/blocks/battery ~/.config/i3/blocks/battery
check_diff config/i3/blocks/cpu ~/.config/i3/blocks/cpu
check_diff config/i3/blocks/datetime ~/.config/i3/blocks/datetime
check_diff config/i3/blocks/load ~/.config/i3/blocks/load
check_diff config/i3/blocks/memory ~/.config/i3/blocks/memory
check_diff config/i3/blocks/network ~/.config/i3/blocks/network
check_diff config/i3/blocks/ssid ~/.config/i3/blocks/ssid
check_diff config/i3/blocks/temperature ~/.config/i3/blocks/temperature
check_diff config/rofi/config ~/.config/rofi/config
mkdir -p ~/.config/systemd/user
check_diff config/systemd/user/emacs.service ~/.config/systemd/user/emacs.service
check_diff config/termite/config ~/.config/termite/config

INSTALL_CMD=
if command -v apt >/dev/null 2>&1; then
  INSTALL_CMD='sudo apt install'
elif command -v pacman >/dev/null 2>&1; then
  INSTALL_CMD='sudo pacman -S'
else
  echo "${INFO} ${RED}✗${RST} Linux packages not installed."
fi

function pkg_install {
  install=$1
  command -v ${install} >/dev/null 2>&1 && {
    echo "${INFO}   ${GREEN}✓${RST} $install already installed."
    return
  }
  shift
  ${INSTALL_CMD} "$install" $@
  if [ $? -eq 0 ]; then
    echo "${INFO}   ${GREEN}✓${RST} $install."
  else
    echo "${INFO}   ${RED}✗${RST} $install."
  fi
}

echo -n "${INFO} Install Linux essential packages? [yN]"
read -n 1 action
echo
action=$(echo "$action" | sed -e 's/\(.*\)/\L\1/')
if [ "$action" == "y" ]; then
  pkg_install dmenu
  pkg_install emacs
  pkg_install exa
  pkg_install fzf
  pkg_install git
  pkg_install go
  pkg_install i3
  pkg_install python
  pkg_install ranger
  pkg_install rofi
  pkg_install mercurial
  pkg_install the_silver_searcher
  pkg_install tmux
  pkg_install yarn '--without-node'
  pkg_install zsh
fi

echo "${INFO} Finished Linux setup."
