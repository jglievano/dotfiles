#!/usr/bin/env bash

AQUA=$(tput setaf 30)
GREEN=$(tput setaf 64)
RED=$(tput setaf 203)
RST=$(tput sgr0)

INFO="${AQUA}[dotfiles]:${RST}"

function brew_tap {
  tap=$1
  shift
  brew tap "$tap" $@
}

function brew_install {
  install=$1
  command -v ${install} >/dev/null 2>&1 && {
    echo "${INFO}   ${GREEN}✓${RST} $install already installed."
    return
  }
  if ! brew ls --versions "$install" >/dev/null; then
    shift
    brew install "$install" $@
    if [ $? -eq 0 ]; then
      echo "${INFO}   ${GREEN}✓${RST} $install."
    else
      echo "${INFO}   ${RED}✗${RST} $install."
    fi
  else
    echo "${INFO}   ${GREEN}✓${RST} $install already installed."
  fi
}

function brew_cask {
  cask=$1
  if ! brew cask ls --versions "$cask" >/dev/null; then
    shift
    brew cask install "$cask" $@
    if [ $? -eq 0 ]; then
      echo "${INFO}   ${GREEN}✓${RST} $1."
    else
      echo "${INFO}   ${RED}✗${RST} $1."
    fi
  else
    echo "${INFO}   ${GREEN}✓${RST} $cask already installed."
  fi
}

STEP_STATUS=0
if command -v brew >/dev/null 2>&1; then
  STEP_STATUS=0
else
  echo "${INFO} Installing Homebrew..."
  mkdir ~/.homebrew && curl -L https://github.com/Homebrew/brew/tarball/master | tar xz --strip 1 -C ~/.homebrew
  STEP_STATUS=$?
fi
if [ $STEP_STATUS -eq 0 ]; then
  echo -n "${INFO} Install macOS essential packages? [yN]: "
  read -n 1 action
  echo
  if [ "$action" == "y" -o "$action" == "Y" ]; then
    brew_tap 'caskroom/fonts'
    brew_tap 'caskroom/homebrew-cask'

    brew_install 'bash'
    brew_install 'bazel'
    brew_install 'colordiff'
    brew_install 'exa'
    brew_install 'ffind'
    brew_install 'fswatch'
    brew_install 'fzf'
    brew_install 'git'
    brew_install 'go'
    brew_install 'graphviz'
    brew_install 'libtool'
    brew_install 'python'
    brew_install 'rbenv'
    brew_install 'reattach-to-user-namespace'
    brew_install 'ruby-build'
    brew_install 'the_silver_searcher'
    brew_install 'tmux'
    brew_install 'tree'
    brew_install 'yarn' '--without-node'
    brew_cask 'emacs'
    brew_cask 'font-iosevka'
  fi
fi

echo "${INFO} Finished macOS setup."
