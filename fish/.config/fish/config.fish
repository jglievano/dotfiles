function prepend_to_path -d "Prepend dir to PATH if it is not already there"
  if test -d $argv[1]
    if not contains $argv[1] $PATH
      set -gx PATH "$argv[1]" $PATH
    end
  end
end

set -gx XDG_BIN_HOME "$HOME/.local/bin"
set -gx XDG_CACHE_HOME "$HOME/.cache"
set -gx XDG_CONFIG_HOME "$HOME/.config"
set -gx XDG_DATA_HOME "$HOME/.local/share"
set -gx XDG_LIB_HOME "$HOME/.local/lib"

set -gx PATH "/sbin"
set -gx ANDROID_HOME "$HOME/android/sdk"
set -gx ANDROID_SDK_ROOT "$HOME/android/sdk"
set -gx ANDROID_AVD_HOME "$HOME/.android/avd"
set -gx LOCAL_BIN "$HOME/.local/bin"
prepend_to_path "/usr/sbin"
prepend_to_path "/bin"
prepend_to_path "/usr/bin"
prepend_to_path "/usr/local/bin"
prepend_to_path "/usr/local/go/bin"
prepend_to_path "$HOME/.cargo/bin"
prepend_to_path "$HOME/.homebrew/bin"
prepend_to_path "$HOME/bin"
prepend_to_path "$ANDROID_HOME/tools"
prepend_to_path "$ANDROID_HOME/tools/bin"
prepend_to_path "$ANDROID_HOME/platform-tools"
prepend_to_path "$LOCAL_BIN"
prepend_to_path "$LOCAL_BIN/flutter/bin"

function v; vim $argv; end
function m; emacsclient -t $argv; end
if type -q exa
  function l; exa $argv; end
  function ll; exa -la $argv; end
  function lx; exa -abghHliS $argv; end
else
  function l; ls -lA1 $argv; end
  function ll; ls -lA $argv; end
  function lx; ls -l $argv; end
end

set editor 'vim'
set -gx ALTERNATE_EDITOR ''
set -gx VISUAL $editor
set -gx EDITOR $editor

if type -q rbenv
  status --is-interactive; and . (rbenv init -|psub)
end

function parse_git_branch
  set -l branch (git branch 2> /dev/null | grep -e '\* ' | sed 's/^..\(.*\)/\1/')
  set -l git_status (git status -s)

  if test -n "$git_status"
    echo (set_color red)$branch(set_color normal)
  else
    echo (set_color green)$branch(set_color normal)
  end
end

function google_prompt
  set_color blue
  printf 'G'
  set_color red
  printf 'o'
  set_color yellow
  printf 'o'
  set_color blue
  printf 'g'
  set_color green
  printf 'l'
  set_color red
  printf 'e'
end

function color_string
  printf '%s%s%s' (set_color $argv[1]) $argv[2] (set_color normal)
end

function fish_prompt
  set last_status $status
  set -l git_dir (git rev-parse --git-dir 2> /dev/null)
  printf '\n'
  set_color normal
  if test -n "$git_dir"
    printf '\u272a %s\n' (parse_git_branch)
  end
  printf '[%s@%s %s]' (color_string yellow $USER) (color_string red (hostname)) (color_string blue (prompt_pwd))
  if test $last_status -eq 0
    printf '$ '
  else
    printf '%s ' (color_string red "\$")
  end
end

function fish_title; true; end

if test -f $HOME/.local.fish
  . $HOME/.local.fish
end

true
