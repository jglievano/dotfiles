#!/usr/bin/env bash

NAME="$1"
git fetch "$NAME" master
git subtree pull --prefix emacs/.emacs.d/site-lisp/"$NAME" "$NAME" master --squash
