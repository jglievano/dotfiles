function prepend_to_path -d "Prepend dir to PATH if it is not already there"
  if test -d $argv[1]
    if not contains $argv[1] $PATH
      set -gx PATH "$argv[1]" $PATH
    end
  end
end

set -gx PATH "/sbin"
set -gx ANDROID_HOME "$HOME/android/sdk"
prepend_to_path "/usr/sbin"
prepend_to_path "/bin"
prepend_to_path "/usr/bin"
prepend_to_path "/usr/local/sbin"
prepend_to_path "/usr/local/go/bin"
prepend_to_path "$HOME/.emacs.d/bin"
prepend_to_path "$HOME/.local/bin"
prepend_to_path "$HOME/.cargo/bin"
prepend_to_path "$HOME/bin"
prepend_to_path "$ANDROID_HOME/tools"
prepend_to_path "$ANDROID_HOME/tools/bin"
prepend_to_path "$ANDROID_HOME/platform-tools"
prepend_to_path "/usr/local/bin"
prepend_to_path "$HOME/.homebrew/bin"

function v; vim $argv; end
function m; emacsclient -t $argv; end
if type -q exa
  function l; exa $argv; end
  function ls; exa $argv; end
  function ll; exa -la $argv; end
  function lx; exa -abghHliS $argv; end
else
  function l; ls -lA1 $argv; end
  function ll; ls -lA $argv; end
  function lx; ls -l $argv; end
end

set editor 'vim'
set -g -x ALTERNATE_EDITOR ''
set -g -x VISUAL $editor
set -g -x EDITOR $editor
if test -z $INSIDE_EMACS
  set -g -x TERM 'screen-256color'
end
set -g -x GOPATH ~/space/go
set -g -x NVM_DIR ~/.nvm

if type -q rbenv
  status --is-interactive; and . (rbenv init -|psub)
end

function nvm
  if test -e $NVM_DIR/nvm.sh
    bass source $NVM_DIR/nvm.sh --no-use ';' nvm $argv
  else
    echo 'nvm not found.'
  end
end

# Prompt stuff.

function fst; set_color -o 970; end
function snd; set_color -o 498; end
function trd; set_color -o 904; end
function dim; set_color -o ccc; end
function off; set_color normal; end

function git::is_stashed
  command git rev-parse --verify --quiet refs/stash >/dev/null
end

function git::get_ahead_count
  echo (command git log ^/dev/null | grep '^commit' | wc -l | tr -d " ")
end

function git::branch_name
  command git symbolic-ref --short HEAD
end

function git::is_touched
  test -n (echo (command git status --porcelain))
end

function in_git
  test -d .git and git rev-parse --is-inside-git-dir >/dev/null 2>&1
end

function git_prompt
  set --local dirty (command git status --porcelain 2> /dev/null | tail -n1)
  set --local branch (git rev-parse --abbrev-ref HEAD ^/dev/null)
  if [ $dirty ]
    set_color magenta
    printf '+> ' $branch
  else
    set_color green
    printf '=> ' $branch
  end
end

function google_prompt
  set_color blue
  printf 'G'
  set_color red
  printf 'o'
  set_color yellow
  printf 'o'
  set_color blue
  printf 'g'
  set_color green
  printf 'l'
  set_color red
  printf 'e'
end

if test -f $HOME/.config/fish/variables.fish
  . $HOME/.config/fish/variables.fish
end

if test -f $HOME/.local.fish
  . $HOME/.local.fish
end

function fish_title
  echo "$PWD | $_" | sed "s|$HOME|~|g"
end

function jgl_right_prompt
  set -l code $status
  test $code -ne 0; and echo (dim)"("(trd)"$code"(dim)") "(off)

  if test -n "$SSH_CONNECTION"
    printf (trd)":"(dim)(hostname)" "(off)
  end

  if git rev-parse 2>/dev/null
    git::is_stashed; and echo (trd)"^"(off)
    printf (snd)"("(begin
      if git::is_touched
        echo (trd)"*"(off)
      else
        echo ""
      end
    end)(fst)(git::branch_name)(snd)(begin
      set -l count (git::get_ahead_count)
      if test $count -eq 0
        echo ""
      else
        echo (trd)"+"(fst)$count
      end
    end)(snd)") "(off)
  end
  printf (dim)(date +%H(fst):(dim)%M(fst):(dim)%S)(off)" "
end

function fish_right_prompt
end

function fish_greeting
  echo (dim)(uname -mnprs)(off)
end

function fish_prompt
  test $status -ne 0;
    and set -l colors 600 900 c00
    or set -l colors 333 666 aaa

  set -l pwd (prompt_pwd)
  set -l base (basename "$pwd")
  set -l user (whoami)

  set -l expr "s|~|"(fst)"^^"(off)"|g; \
               s|/|"(snd)"/"(off)"|g;  \
               s|"$base"|"(fst)$base(off)" |g"

  echo -n "$user@$pwd"

  if test -n "$SSH_CONNECTION"
    echo -n "\$"
  end

  echo -n "\$ "
end

true
