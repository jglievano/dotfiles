#!/usr/bin/env bash

NAME="$1"
URL="$2"

git remote add -f "$NAME" "$URL"
