stow_to () {
  declare dst="$1" pkg="$2"
  echo "Installing ${pkg:?} to ${dst:?}"
  stow --verbose=1 -t "${dst:?}" "$pkg"
}

main () {
  command -v stow &>/dev/null || \
      { printf "stow <- dependency missing."; exit 1; }
  target="${INSTALL_DST}"
  [[ -z "${INSTALL_DST}" ]] && target="${HOME}"
  stow_to "$target" bash
  stow_to "$target" git
  if [[ "$(uname -s)" == Linux* ]]; then
    stow_to "$target" i3
    stow_to "$target" i3status
    stow_to "$target" systemd
    stow_to "$target" termite
    stow_to "$target" xorg
 fi
}

main
