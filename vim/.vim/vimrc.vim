"
"     ██▒   █▓ ██▓ ███▄ ▄███▓ ██▀███   ▄████▄
"    ▓██░   █▒▓██▒▓██▒▀█▀ ██▒▓██ ▒ ██▒▒██▀ ▀█
"     ▓██  █▒░▒██▒▓██    ▓██░▓██ ░▄█ ▒▒▓█    ▄
"      ▒██ █░░░██░▒██    ▒██ ▒██▀▀█▄  ▒▓▓▄ ▄██▒
"       ▒▀█░  ░██░▒██▒   ░██▒░██▓ ▒██▒▒ ▓███▀ ░
"       ░ ▐░  ░▓  ░ ▒░   ░  ░░ ▒▓ ░▒▓░░ ░▒ ▒  ░
"       ░ ░░   ▒ ░░  ░      ░  ░▒ ░ ▒░  ░  ▒
"         ░░   ▒ ░░      ░     ░░   ░ ░
"          ░   ░         ░      ░     ░ ░
"         ░                           ░

set encoding=utf-8
scriptencoding utf-8

" Set shell to sh because I use fish mostly.
if &shell =~# 'fish$'
  set shell=sh
endif

" Install bundles
if filereadable(expand('~/.vim/bundles.vim'))
  source ~/.vim/bundles.vim
endif

" Ensure ftdetect et al work
filetype plugin indent on

" ---Color theme----------------------------------------------------------------
syntax on
set background=dark
colorscheme gruvbox
let g:gruvbox_contrast_dark='hard'
hi Visual term=reverse cterm=reverse guibg=Grey

" ---Better defaults------------------------------------------------------------
set autoindent
set autoread
set noautowrite
set backspace=indent,eol,start
set backupcopy=yes
set backupdir=~/.vim/backup//
set colorcolumn=80,100
set completeopt=menu
set nocursorline
set directory=~/.vim/swap//
set expandtab
set exrc
set fillchars=vert:\ ,stl:\ ,stlnc:\
set guifont=Operator\ Mono\ Light:h14
set guioptions-=L
set nohidden
set history=1000
set hlsearch
set ignorecase
set incsearch
set laststatus=2
set nolazyredraw
set nolinebreak
set list
set listchars=tab:▸\ ,extends:❯,precedes:❮,trail:▫
set matchtime=3
set modelines=0
set mouse-=a
set nonumber
set ruler
set scrolloff=3
set secure
set shiftround
set shiftwidth=2
set showcmd
set showmatch
set statusline=%=%P\ %f\ %m
set noshowmode
set secure
set showbreak=↪
set showtabline=2
set smartcase
set splitbelow
set splitright
set softtabstop=2
set t_vb=
set tabstop=2
set termencoding=utf-8
set textwidth=0
set notimeout
set title
set ttimeout
set ttimeoutlen=10
set ttyfast
set t_Co=256
set undofile
set undoreload=10000
set undodir=~/.vim/undo//
set visualbell
set nowrap
set wrapmargin=0
set wildchar=<Tab>
set wildignore+=log/**,node_module/**,target/**,tmp/**,*.rdc
set wildignore+=.hg,.git,.svn
set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg
set wildignore+=*.u,*.obj,*.exe,*.dll
set wildignore+=.DS_Store
set wildignore+=*.pyc
set wildignore+=*.orig
set wildmenu
set wildmode=list:longest,full

if !isdirectory(expand(&undodir))
  call mkdir(expand(&undodir), 'p')
endif
if !isdirectory(expand(&backupdir))
  call mkdir(expand(&backupdir), 'p')
endif
if !isdirectory(expand(&directory))
  call mkdir(expand(&directory), 'p')
endif

" Trailing Whitespace ----------------------------------------------------------
augroup trailing
  au!
  au InsertEnter * :set listchars-=trail:⌴
  au InsertLeave * :set listchars+=trail:⌴
augroup END

" ---Appearance-----------------------------------------------------------------
highlight ColorColumn ctermbg=255

iabbrev zxa ಠ_ಠ
iabbrev zxc ✓
iabbrev zxx ✘

" ---Extensions-----------------------------------------------------------------
augroup extensionsgroup
  autocmd!
  " ejs.
  autocmd BufRead,BufNewFile *.ejs set filetype=html
  " fish.
  autocmd BufRead,BufNewFile *.fish set filetype=fish
  " fdoc is yaml.
  autocmd BufRead,BufNewFile *.fdoc set filetype=yaml
  " md is markdown.
  autocmd BufRead,BufNewFile *.md set filetype=markdown
  autocmd BufRead,BufNewFile *.md set spell
  " borg is gcl.
  autocmd BufRead,BufNewFile *.borg set filetype=gcl
  " gyp is python.
  autocmd BufRead,BufNewFile *.gyp set filetype=python
  autocmd BufRead,BufNewFile *.gypi set filetype=python
  " BUCK/BUILD is python
  autocmd BufRead,BufNewFile BUILD set filetype=python
  autocmd BufRead,BufNewFile BUCK set filetype=python
  " swift specificity
  autocmd BufRead,BufNewFile *.swift set filetype=swift
  " Needed to override matlab default.
  autocmd BufRead,BufNewFile *.m set filetype=objc
  " automatically rebalance windows on vim resize
  autocmd VimResized * :wincmd =
augroup END

" Fix cursor in TMUX
if exists('$TMUX')
  let &t_SI="\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
  let &t_EI="\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
else
  let &t_SI="\<Esc>]50;CursorShape=1\x7"
  let &t_EI="\<Esc>]50;CursorShape=0\x7"
endif

" Don't copy the contents of an overwritten selection
vnoremap p "_dP

" Vim-easy-align
xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

" Ctrl-P
let g:ctrlp_match_window='bottom,order:btt,min:1,max:10,results:10'

" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list=1
let g:syntastic_loc_list_height=3
let g:syntastic_auto_loc_list=0
let g:syntastic_check_on_open=0
let g:syntastic_check_on_wq=0
let g:syntastic_ruby_checkers=['rubocop']
let g:syntastic_go_checkers=['gofmt']

" UltiSnips
let g:UltiSnipsExpandTrigger='<c-j>'
let g:UltiSnipsEditSplit='vertical'

" ---Vim-Tmux-Navigator---------------------------------------------------------
if exists('$TMUX')
  function! TmuxOrSplitSwitch(wincmd, tmuxdir)
    let previous_winnr=winnr()
    silent! execute 'wincmd ' . a:wincmd
    if previous_winnr == winnr()
      call system('tmux select-pane -' . a:tmuxdir)
      redraw!
    endif
  endfunction

  let previous_title=substitute(system("tmux display-message -p '#{pane_title}'"), '\n', '', '')
  let &t_ti="\<Esc>]2;vim\<Esc>\\" . &t_ti
  let &t_te="\<Esc>]2;". previous_title . "\<Esc>\\" . &t_te

  nnoremap <silent> <C-h> :call TmuxOrSplitSwitch('h', 'L')<cr>
  nnoremap <silent> <C-j> :call TmuxOrSplitSwitch('j', 'D')<cr>
  nnoremap <silent> <C-k> :call TmuxOrSplitSwitch('k', 'U')<cr>
  nnoremap <silent> <C-l> :call TmuxOrSplitSwitch('l', 'R')<cr>
else
  map <C-h> <C-w>h
  map <C-j> <C-w>j
  map <C-k> <C-w>k
  map <C-l> <C-w>l
endif

" ---Airline--------------------------------------------------------------------
let g:airline_powerline_fonts=0
let g:airline_theme='lucius'

" ---FZF------------------------------------------------------------------------
let $FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .git'

" ---Sourcekit-LSP--------------------------------------------------------------
if executable('sourcekit-lsp')
  augroup sourcekit
    au User lsp_setup call lsp#register_server({
          \ 'name': 'sourcekit-lsp',
          \ 'cmd': {server_info->['sourcekit-lsp']},
          \ 'whitelist': ['swift'],
          \ })
  augroup END
endif
let g:lsp_diagnostics_enabled=0

" ---RLS------------------------------------------------------------------------
if executable('rls')
  augroup rls
    au User lsp_setup call lsp#register_server({
          \ 'name': 'rls',
          \ 'cmd': {server_info->['rustup', 'run', 'stable', 'rls']},
          \ 'whitelist': ['rust'],
          \ })
  augroup END
endif

" ---ALE------------------------------------------------------------------------
let g:ale_fixers={
      \'*': ['remove_trailing_lines', 'trim_whitespace'],
      \'swift': ['swiftformat'],
      \'css': ['prettier'],
      \'scss': ['prettier'],
      \}
let g:ale_linters={
      \'css': ['stylelint'],
      \'scss': ['sass-lint', 'stylelint'],
      \}
let g:ale_completion_enabled=1

" ---Prose-mode-----------------------------------------------------------------
function! ProseMode()
  call goyo#execute(0, [])
  set spell noci nosi noai nolist noshowmode noshowcmd
  set complete+=s
endfunction
command! ProseMode call ProseMode()
nmap \p :ProseMode<CR>

" Local Vim.
let g:localvimrc_ask=0

if filereadable(expand('~/.vim/keybindings.vim'))
  source ~/.vim/keybindings.vim
endif

" Run local configuration.
if filereadable(expand('~/.vimrc.local'))
  source ~/.vimrc.local
endif
