call plug#begin('~/.vim/plugged')
Plug 'airblade/vim-gitgutter'
Plug 'ajh17/VimCompletesMe'
Plug 'aklt/plantuml-syntax'
Plug 'arcticicestudio/nord-vim'
Plug 'austintaylor/vim-indentobject'
Plug 'cespare/vim-toml', { 'for': 'toml' }
Plug 'christoomey/vim-tmux-navigator'
Plug 'csexton/trailertrash.vim'
Plug 'dag/vim-fish', { 'for': 'fish' }
Plug 'digitaltoad/vim-pug', { 'for': 'pug' }
Plug 'eagletmt/ghcmod-vim'
Plug 'embear/vim-localvimrc'
Plug 'fatih/vim-go', { 'for': 'go' }
Plug 'justinmk/vim-sneak'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/vim-easy-align'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'kchmck/vim-coffee-script'
Plug 'keith/swift.vim', { 'for': 'swift' }
Plug 'kien/rainbow_parentheses.vim'
Plug 'leafgarland/typescript-vim'
Plug 'majutsushi/tagbar'
Plug 'mattn/emmet-vim'
Plug 'morhetz/gruvbox'
Plug 'mustache/vim-mustache-handlebars'
Plug 'mxw/vim-jsx', { 'for': 'javascript' }
Plug 'pangloss/vim-javascript', { 'for': 'javascript' }
Plug 'posva/vim-vue', { 'for': 'vue' }
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'reedes/vim-colors-pencil'
Plug 'rking/ag.vim'
Plug 'rix0rrr/vim-gcl', { 'for': 'gcl' }
Plug 'rust-lang/rust.vim', { 'for': 'rust' }
Plug 'ryanolsonx/vim-lsp-python'
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'scrooloose/syntastic'
Plug 'scrooloose/vim-slumlord'
Plug 'shougo/neocomplete.vim'
Plug 'shougo/vimproc.vim', { 'do': 'make' }
Plug 'sjl/gundo.vim'
Plug 'terryma/vim-expand-region'
Plug 'tomtom/tcomment_vim'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-dispatch'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'udalov/kotlin-vim', { 'for': 'kotlin' }
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'vim-ruby/vim-ruby'
Plug 'vim-scripts/Align'
Plug 'vim-syntastic/syntastic'
Plug 'whatyouhide/vim-gotham'
Plug 'w0rp/ale'
call plug#end()
