.PHONY: emacs_init emacs_update

emacs_init:
	@./emacs-batch-add-subtrees.sh

emacs_update:
	@./emacs-batch-update-subtrees.sh
