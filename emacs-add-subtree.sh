#!/usr/bin/env bash

NAME="$1"
URL="$2"

./emacs-register-subtree.sh "$NAME" "$URL"
if [[ -d "emacs/.emacs.d/site-lisp/$NAME" ]]; then
  exit
fi
git subtree add --prefix emacs/.emacs.d/site-lisp/"$NAME" "$NAME" master --squash
